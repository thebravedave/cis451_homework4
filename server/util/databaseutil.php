<?php
/**
 * Created by IntelliJ IDEA.
 * User: david
 * Date: 11/1/18
 * Time: 3:58 PM
 */

class DatabaseUtil{
    static function checkDatabaseConnectivity(){
        $config = DatabaseUtil::getDatabaseConfigurationDetails();
        $bool = true;
        $mysqli = null;
        try {
            $mysqli = new mysqli($config->server, $config->username, $config->password, $config->database);
        } catch (Exception $e) {
            $bool = false;
        }
        if($mysqli == null || $mysqli->connect_error == null || $mysqli->connect_error == "NULL")
            $bool = false;
        return $bool;
    }
    static function getDatabaseConfigurationDetails(){

        $databaseConfigurationInformation = new DatabaseConfigurationInformation();

        if(isset($_SESSION['databaselocation']))
            $fileName = $_SESSION['databaselocation'];
        else{
            $locationName = "../conf/databaseconfigurationlocation.php";
            try{
                $databaseLocationContents = file($locationName);
            }
            catch(Exception $e){
                return null;
            }
            $locationString = "";
            foreach($databaseLocationContents as $key => $value)
            {
                if(strpos($value, "location") !== false)
                    $locationString = $value;
            }
            if($locationString == "")
                return null;
            $locationArray = explode("=", $locationString);
            if(sizeof($locationArray) != 2)
                return null;
            $fileName = $locationArray[1];
            $_SESSION['databaselocation'] = $fileName;
        }



        $serverstring = "";
        $databasestring = "";
        $usernamestring = "";
        $passwordstring = "";
        $portString = "";
        $fileName = trim($fileName);


        try{
            $contents = file($fileName);
        }
        catch(Exception $e){
            return null;
        }
        foreach($contents as $key => $value)
        {
            if(strpos($value, "server") !== false)
                $serverstring = $value;
            if(strpos($value, "database") !== false)
                $databasestring = $value;
            if(strpos($value, "username") !== false)
                $usernamestring = $value;
            if(strpos($value, "password") !== false)
                $passwordstring = $value;
            if(strpos($value, "port") !== false)
                $portString = $value;
        }
        $serverArray = explode("=", $serverstring);
        $server = trim($serverArray[1]);
        $databaseArray = explode("=", $databasestring);
        $database = trim($databaseArray[1]);
        $userArray = explode("=", $usernamestring);
        $user = trim($userArray[1]);
        $passwordArray = explode("=", $passwordstring);
        $password = trim($passwordArray[1]);

        $portArray = explode("=", $portString);
        $port = $portArray[1];

        $databaseConfigurationInformation->server = $server;
        $databaseConfigurationInformation->database = $database;
        $databaseConfigurationInformation->username = $user;
        $databaseConfigurationInformation->password = $password;
        $databaseConfigurationInformation->port = $port;


        return $databaseConfigurationInformation;
    }
}