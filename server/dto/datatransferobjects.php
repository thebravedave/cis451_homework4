<?php
/***************************************************************************************
 ****************************************************************************************
 *     All code shown copyright of Binary Web Design LLC. Copyright 2013. ***************
 ****************************************************************************************
 ***************************************************************************************/

class Location{
    public $state = "OR";
    public $city = "Eugene";
    public $zipCode = "97402";
}
class Customer{

    public $customerNumber;
    public $firstName;
    public $lastName;
    public $lastNameFirstName;
    public $company;
    public $address1;
    public $address2;
    public $city;
    public $state;
    public $zipcode;
    public $phone;

}

class Order{
    public $orderNumber;
    public $orderDate;
    public $customerNumber;
    public $totalPrice;
    public $numberOfItems;
    public $shippingInstructions;
    public $backLog;
    public $postOfficeNumber;
    public $shipDate;
    public $shipWeight;
    public $shipCharge;
    public $paidDate;
}

class CustomerOrder{
    public $order;
}
class CustomersOrders{
    public $customer;
    public $customersOrders = array();
}

class DatabaseConfigurationInformation{
    public $server;
    public $database;
    public $username;
    public $password;
    public $port;
}

class Success{
    public $success = true;
    public $message;
}

?>
