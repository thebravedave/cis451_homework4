<?php
/**
 * Created by IntelliJ IDEA.
 * User: david
 * Date: 11/1/18
 * Time: 3:55 PM
 */

class CustomersDAO
{
    private $mysqli;

    function __construct()
    {
        $databaseInformation = DatabaseUtil::getDatabaseConfigurationDetails();
        if ($databaseInformation == null)
            return;

        $this->site_dbServer = $databaseInformation->server;
        $this->site_dbName = $databaseInformation->database;
        $this->site_dbUser = $databaseInformation->username;
        $this->site_dbPassword = $databaseInformation->password;
        $this->port = $databaseInformation->port;
        $port = (int) $this->port;
        $mysqli = new mysqli($this->site_dbServer, $this->site_dbUser, $this->site_dbPassword, $this->site_dbName, $port);
        $this->mysqli = $mysqli;
    }


    public function getAllCustomers(){
        $customers = array();
        $query = "SELECT * FROM customer ORDER BY lname ASC";
        $stmt = $this->mysqli->prepare($query);
        if($stmt) {
            $stmt->execute();
            $stmt->bind_result($customerNumber, $firstName, $lastName, $company, $address1, $address2, $city, $state, $zipcode, $phone);
            $stmt->store_result();
            while ($stmt->fetch())
            {

                $customer = new Customer();
                $customer->customerNumber = $customerNumber;
                $customer->firstName = $firstName;
                $customer->lastName = $lastName;
                $customer->lastNameFirstName = $lastName . ", " . $firstName;
                $customer->company = $company;
                $customer->address1 = $address1;
                $customer->address2 = $address2;
                $customer->city = $city;
                $customer->state = $state;
                $customer->zipcode = $zipcode;
                $customer->phone = $zipcode;
                $customers[count($customers)] = $customer;

            }
            $stmt->free_result();
            $stmt->close();
        }
        else{
            var_dump($this->mysqli->error);
        }

        return $customers;
    }

    public function getCustomerInformationById($customerId){
        $customersOrders = new CustomersOrders();
        $query = "
          SELECT customer.fname, customer.lname, IFNULL(orders.order_num, 'No Orders'), orders.order_date, SUM(items.total_price) as 'Total Price', COUNT(items.item_num) as 'Number of Items' FROM stores7.orders
          RIGHT JOIN customer ON customer.customer_num = orders.customer_num
          LEFT JOIN items ON items.order_num = orders.order_num
          WHERE customer.customer_num = ?
          GROUP BY orders.order_num
          ORDER BY orders.order_date";
        $stmt = $this->mysqli->prepare($query);
        if($stmt) {
            $stmt->bind_param('i', $customerId);
            $stmt->execute();
            $stmt->bind_result( $firstName,$lastName, $orderNum, $orderDate, $totalPrice, $numberOfItems);
            $stmt->store_result();

            while ($stmt->fetch())
            {

                $customer = new Customer();
                $customer->customerNumber = $customerId;
                $customer->firstName = $firstName;
                $customer->lastName = $lastName;


                $order = new Order();
                $order->orderNumber = $orderNum;
                $order->orderDate = $orderDate;
                $order->totalPrice = $totalPrice;
                $order->numberOfItems = $numberOfItems;



                $customerOrder = new CustomerOrder();
                $customersOrders->customer = $customer;
                $customerOrder->order = $order;

                if($orderNum != "No Orders"){
                    $customersOrders->customersOrders[count($customersOrders->customersOrders)] = $customerOrder;
                }





            }
            $stmt->free_result();
            $stmt->close();
        }
        else{
            var_dump($this->mysqli->error);
        }

        return $customersOrders;
    }


}