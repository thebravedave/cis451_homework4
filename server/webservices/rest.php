<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');



session_start();

include_once("../dto/datatransferobjects.php");
include_once("../managers/storemanager.php");
include_once("../dal/customersdao.php");
include_once("../util/databaseutil.php");


require '../lib/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim(array(
    'debug' => true
));



$app->get('/customers/', function() use($app) {

    $storeManager = new StoreManager();


    $customers = $storeManager->getAllCustomers();

    $success = new Success();
    $success->message = $customers;
    $json = json_encode($success);
    echo $json;

}); //customers/'

$app->get('/customer/information/:customerId', function($customerId) use($app) {

    $storeManager = new StoreManager();


    $customers = $storeManager->getCustomerInformationByCustomerId($customerId);

    $success = new Success();
    $success->message = $customers;
    $json = json_encode($success);
    echo $json;

}); //customers/'


$app->run();

