'use strict';

/* Services */
//////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////W E A T H E R      S E R V I C E S /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

var categoryServices = angular.module('CustomerServices', []);
categoryServices.factory('CustomerService', function($http) {
    return {
        getCustomers: function(){
            return $http.get('./server/webservices/rest.php/customers');
        },
        getCustomerInformationByCustomerId: function(customerId){
            return $http.get('./server/webservices/rest.php/customer/information/' + customerId);
        },
        getSatelliteData: function(state, city){
            return $http.get('./server/webservices/rest.php/forecast/satellite/' + state + '/' + city);
        },
        getWebcamData: function(state, city){
            return $http.get('./server/webservices/rest.php/forecast/webcams/' + state + '/' + city);
        }
    }
});
