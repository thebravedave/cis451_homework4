'use strict';

angular.module('CustomerDirectives', ['ui.bootstrap'])
.directive('customerDirectiveContainer', function() {
    return {
        restrict: "E",
        scope: {},
        link: function (scope, element, $location) {},
        controller: function($rootScope, $scope, $location, CustomerService){
            init();
            function init(){
                $scope.customerId = null;
                getCustomers();
                initializeEventListeners();
            }
            function getCustomers(){
                CustomerService.getCustomers().success(function(data){
                    if(data.success == true){

                        $scope.customers = data.message;

                    }

                }).error(function(){
                    //todo then there was an error
                });
            }
            function initializeEventListeners(){
                $scope.$watch('customerId', function(data){

                    if($scope.customerId != null){
                        CustomerService.getCustomerInformationByCustomerId($scope.customerId).success(function(data){

                            $scope.customersOrders = data.message;
                        }).error(function(){

                        });
                    }
                });



            }

        },
        templateUrl:"./partials/directivetemplates/customerdirectivecontainer.html"
    }
})
