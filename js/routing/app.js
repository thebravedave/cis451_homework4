'use strict';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////M O D U L E   A P P    R E G I S T R A T I O N/////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var customerApp = angular.module('customerApp', [
    'ngRoute',
    'CustomerController',
    'CustomerServices',
    'CustomerDirectives',
    'ngTouch'

]);

customerApp.filter("sanitize", ['$sce', function($sce) {
    return function(htmlCode){
        return $sce.trustAsHtml(htmlCode);
    }
}]);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////          M O D U L E   R O U T E R           /////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
customerApp.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('!');
        $locationProvider.html5Mode(false);
        $routeProvider.
            when('/customerinformationcenter', {
                templateUrl: 'partials/controllertemplates/customersinformation.html',
                controller: 'CustomerController',
                access: {
                    name: "login",
                    isFree: true
                }
            }).
            otherwise({
                redirectTo: '/customerinformationcenter/'
            });
    }]
);





