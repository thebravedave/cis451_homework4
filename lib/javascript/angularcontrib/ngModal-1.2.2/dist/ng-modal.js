(function() {
  var app;

  app = angular.module("ngModal", []);

  app.provider("ngModalDefaults", function() {
    return {
      options: {
        closeButtonHtml: "<span class='ng-modal-close-x'></span>"
      },
      $get: function() {
        return this.options;
      },
      set: function(keyOrHash, value) {
        var k, v, _results;
        if (typeof keyOrHash === 'object') {
          _results = [];
          for (k in keyOrHash) {
            v = keyOrHash[k];
            _results.push(this.options[k] = v);
          }
          return _results;
        } else {
          return this.options[keyOrHash] = value;
        }
      }
    };
  });

  app.directive('modalDialog', [
    'ngModalDefaults', '$sce', function(ngModalDefaults, $sce) {
      return {
        restrict: 'E',
        scope: {
          show: '=',
          variable:'=',
          cartItems:'=',
            dialogShown:'=',
          dialogTitle: '@',
          onClose: '&?'
        },
        replace: true,
        transclude: true,
        link: function(scope, element, attrs) {

            /*CARTANGULAR ADDED FUNCTIONALITY*/

            var getCrossBrowserElementCoords = function (mouseEvent)
            {
                var result = {
                    x: 0,
                    y: 0
                };

                if (!mouseEvent)
                {
                    mouseEvent = window.event;
                }

                if (mouseEvent.pageX || mouseEvent.pageY)
                {
                    result.x = mouseEvent.pageX;
                    result.y = mouseEvent.pageY;
                }
                else if (mouseEvent.clientX || mouseEvent.clientY)
                {
                    result.x = mouseEvent.clientX + document.body.scrollLeft +
                        document.documentElement.scrollLeft;
                    result.y = mouseEvent.clientY + document.body.scrollTop +
                        document.documentElement.scrollTop;
                }

                if (mouseEvent.target)
                {
                    var offEl = mouseEvent.target;
                    var offX = 0;
                    var offY = 0;

                    if (typeof(offEl.offsetParent) != "undefined")
                    {
                        while (offEl)
                        {
                            offX += offEl.offsetLeft;
                            offY += offEl.offsetTop;

                            offEl = offEl.offsetParent;
                        }
                    }
                    else
                    {
                        offX = offEl.x;
                        offY = offEl.y;
                    }

                    result.x -= offX;
                    result.y -= offY;
                }

                return result;
            };

            var getMouseEventResult = function (mouseEvent, mouseEventDesc)
            {
                var coords = getCrossBrowserElementCoords(mouseEvent);
                return coords;
            };
            scope.onMouseOver = function ($event) {
               var coords = getMouseEventResult($event, "Mouse over");
               if(scope.coords == undefined)
                    scope.coords = coords;
               else{
                   //todo check to see if they have gone past the limit
                   if(coords.x < (scope.coords.x - 50) || coords.x > (scope.coords.x + 50) || coords.y < (scope.coords.y - 50) || coords.y > (scope.coords.y + 50)){
                     //  scope.dialogShown = false;
                       scope.hideModal();
                   }
               }
            };

            function getMousePosition(){

            }
            /*END CARTANGULAR ADDED FUNCTIONALITY*/






            getMousePosition();
            scope.mouseMoved = function(){

            };

          var setupCloseButton, setupStyle;
          setupCloseButton = function() {
            return scope.closeButtonHtml = $sce.trustAsHtml(ngModalDefaults.closeButtonHtml);
          };
          setupStyle = function() {
            scope.dialogStyle = {};
            if (attrs.width) {
              scope.dialogStyle['width'] = attrs.width;
            }
            if (attrs.height) {
              return scope.dialogStyle['height'] = attrs.height;
            }
          };
          scope.hideModal = function() {
              scope.dialogShown = false;
            return scope.show = false;
          };
          scope.$watch('show', function(newVal, oldVal) {
            if (newVal && !oldVal) {
              document.getElementsByTagName("body")[0].style.overflow = "hidden";
            } else {
              document.getElementsByTagName("body")[0].style.overflow = "";
            }
            if ((!newVal && oldVal) && (scope.onClose != null)) {
              return scope.onClose();
            }
          });
          setupCloseButton();
          return setupStyle();



        },
        template: "<div class='ng-modal' ng-show='show'>\n  <div class='ng-modal-overlay'ng-mousemove='onMouseOver($event)' ng-click='hideModal()'></div>\n  <div class='ng-modal-dialog' ng-style='dialogStyle'>\n    <span class='ng-modal-title' ng-show='dialogTitle && dialogTitle.length' ng-bind='dialogTitle'></span>\n    <div class='ng-modal-close' ng-click='hideModal()'>\n      <div ng-bind-html='closeButtonHtml'></div>\n    </div>\n    <div class='ng-modal-dialog-content' ng-transclude></div>\n  </div>\n</div>"
      };
    }
  ]);

}).call(this);
