This is a project for David Pugh's CIS 451 Databases class, Homework 4

My project uses AngularJs front end, where the only thing on the front end UI is javascript and html.
The javascript makes calls to the server side backend which is a RESTful webservice backend.  
The backend is seperated into layers/modules using the concept known as "seperation of concerns".
The back end layers are the webservice layer (RESTful webservice) which communicates with the business object
layer.  The business object layer communicates with the webservice layer and the data access layer.
This code pasted below is from the dataaccess layer.